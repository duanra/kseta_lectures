{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solutions to\n",
    "# Tutorial \"Algorithmic Methods for Network Analysis with NetworKit\" (Part 1)\n",
    "Welcome to the hands-on session of our tutorial! This tutorial is based on the user guide of NetworKit, our network analysis software. You will learn in this tutorial how to use NetworKit for fundamental tasks in network analysis.\n",
    "\n",
    "NetworKit can run in your browser (thanks to IPython notebooks) and is still very fast (thanks to C++ code in the background). It is easy to mix text with code and solutions in this environment. Thus, you should be able to obtain your results in a convenient and quick manner. This is not only true for the rather small graphs we use for this tutorial, but for larger instances in production runs as well. In particular you can mix text, code, plots and other rich media in this environment. Since this allows a simplified execution and interpretation of experiments, the interactive approach followed by NetworKit can simplify the cyclic algorithm engineering process significantly (without compromising algorithm performance).\n",
    "\n",
    "## Preparation\n",
    "Let's start by making NetworKit available in your session. Click into the cell below and hit space-return or click the \"Play\" button or select \"Cell -> Run\" in the menu."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from networkit import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case a Python warning appears that recommends an update to Python 3.4, simply ignore it for this tutorial. Python 3.3 works just as fine for our purposes.\n",
    "\n",
    "IPython lets us use familiar shell commands in a Python interpreter. Use one of them now to change into the directory where you unpacked `tutorial.zip`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd DIR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading Graphs\n",
    "Let us start by reading a network from a file on disk: [PGPgiantcompo.graph](http://www.cc.gatech.edu/dimacs10/archive/data/clustering/PGPgiantcompo.graph.bz2). NetworKit supports a number of popular file formats. There is a convenient function in the top namespace which reads a graph from a file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = readGraph(\"input/PGPgiantcompo.graph\", Format.METIS)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the course of this tutorial, we are going to work (among others) on the `PGPgiantcompo` network, a social network/web of trust in which nodes are PGP keys and an edge represents a signature from one key on another (web of trust). It is distributed with NetworKit as a good starting point.\n",
    "\n",
    "## The Graph Object\n",
    "\n",
    "`Graph` is the central class of NetworKit. An object of this type represents an optionally weighted network. In this tutorial we work with undirected graphs, but NetworKit supports directed graphs as well.\n",
    "\n",
    "Let us inspect several of the methods which the class provides. Maybe the most basic information is the number of nodes and edges in as well as the name of the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = G.numberOfNodes()\n",
    "m = G.numberOfEdges()\n",
    "print(n, m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G.toString()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NetworKit stores nodes simply as integer indices. Edges are pairs of such indices. The following prints the indices of the first ten nodes and edges, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "V = G.nodes()\n",
    "print(V[:10])\n",
    "E = G.edges()\n",
    "print(E[:10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another very useful feature is to determine if an edge is present and what its weight is. In case of unweighted graphs, edges have the default weight 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "edgeExists = G.hasEdge(42,11)\n",
    "if edgeExists:\n",
    "    print(\"Weight of existing edge:\", G.weight(42,11))\n",
    "print(\"Weight of nonexisting edge:\", G.weight(42,12))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many graph algorithms can be expressed with iterators over nodes or edges. As an example, let us iterate over the nodes to determine how many of them have more than 100 neighbors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "count = 0 # counts number of nodes with more than 100 neighbors\n",
    "for v in G.nodes():\n",
    "    if G.degree(v) > 100:\n",
    "        count = count + 1\n",
    "print(\"Number of nodes with more than 100 neighbors: \", count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interesting Features of a Network\n",
    "Let us become more concrete: In the talk that accompanies this tutorial you learned about basic network features. Go back to the 'Analytics' section of the slides and answer the following questions within the box below, including the code which found your answer (click on the box to enter text). If you need information on method prototypes, you have at least two options: Use the built-in code completion (tab) or the project website, which offers documentation in the form of an automatically generated reference: https://networkit.iti.kit.edu/documentation/ (Python/C++ Documentation in the left navigation bar).\n",
    "\n",
    "**After** you answered the questions, go on with Tutorial #2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Q&A Session #1\n",
    "1. Who (which vertex) has the least/most 'friends' and how many?\n",
    "**Answer:**\n",
    "\n",
    "2. How many neighbors does a vertex have on average?\n",
    "**Answer:** \n",
    "\n",
    "3. Does the degree distribution follow a power law?\n",
    "**Answer:** \n",
    "\n",
    "4. How often is the friend of a friend also a friend? Let's go for the average fraction here, other definitions are possible...\n",
    "**Answer:**\n",
    "\n",
    "5. How many connected components does the graph have?\n",
    "**Answer:** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Enter code for Q&A Session #1 here\n",
    "argmin = 0\n",
    "argmax = 0\n",
    "mini = G.numberOfNodes()\n",
    "maxi = 0\n",
    "degsum = 0\n",
    "for v in G.nodes():\n",
    "    deg = G.degree(v)\n",
    "    degsum = degsum + deg\n",
    "    if deg < mini:\n",
    "        mini = deg\n",
    "        argmin = v\n",
    "    if deg > maxi:\n",
    "        maxi = deg\n",
    "        argmax = v\n",
    "\n",
    "avgdeg = degsum / G.numberOfNodes()\n",
    "\n",
    "# Answers to 1-1) and 1-2)\n",
    "print(\"1-1a) min: \", mini, \" / argmin: \", argmin)\n",
    "print(\"1-1b) max: \", maxi, \" / argmax: \", argmax)\n",
    "print(\"1-2)  avg: \", avgdeg)\n",
    "\n",
    "# Answer to 1-3)\n",
    "dd = sorted(centrality.DegreeCentrality(G).run().scores(), reverse=True)\n",
    "import powerlaw\n",
    "fit = powerlaw.Fit(dd)\n",
    "print(\"1-3) Power law fit: \", fit.alpha)\n",
    "    \n",
    "# Answer to 1-4)\n",
    "alcc = centrality.LocalClusteringCoefficient(G)\n",
    "print(\"1-4)  Exact average local clustering coefficient: \", alcc)\n",
    "\n",
    "# Answer to 1-5)\n",
    "conncomp = components.ConnectedComponents(G)\n",
    "conncomp.run()\n",
    "print(\"1-5)  Number of components: \", conncomp.numberOfComponents())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
